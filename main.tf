resource "random_password" "password" {
  // Create a new password with special chars and 16 characters
  length  = 16
  special = true
}

resource "google_sql_database_instance" "master" {
  // Create a new sql database with variables.tf content
  name = var.instance_name
  database_version = var.database_version
  project = var.gcp_project
  region = var.region

  // We allow internet access only for lab purpose
  settings {
    tier = "db-f1-micro"
    ip_configuration {
      ipv4_enabled = true
      authorized_networks {
        name  = "internet"
        value = "0.0.0.0/0"
      }
    }
  }
}

resource "google_sql_user" "users" {
  // Create the database user
  name = var.username
  project = var.gcp_project
  instance = google_sql_database_instance.master.name
  password = random_password.password.result
}

resource "vault_generic_secret" "example" {
  // Put the password in vault
  path = "secret/demo-sql-ldubocquet"
  data_json = <<EOF
{
  "user": "${var.username}",
  "password": "${random_password.password.result}"
}
EOF
}

